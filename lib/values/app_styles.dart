import 'package:flutter/cupertino.dart';
import 'package:flutter_app1/values/app_colors.dart';

/// style of Text, Button

class FontFamily {
  static const inter = 'Inter';
  static const sfpro = 'SfPro';
}

class AppStyles {
  static TextStyle namepage = const TextStyle(
      fontFamily: FontFamily.inter,
      fontSize: 20,
      color: AppColors.aqua,
      fontWeight: FontWeight.w400);
  static TextStyle title = const TextStyle(
      fontFamily: FontFamily.inter,
      fontSize: 18,
      color: AppColors.primaryColor,
      fontWeight: FontWeight.w500);
  static TextStyle order = const TextStyle(
      fontFamily: FontFamily.inter,
      fontSize: 15,
      color: AppColors.grey,
      fontWeight: FontWeight.w300);
  static TextStyle button = const TextStyle(
      fontFamily: FontFamily.sfpro,
      fontSize: 15,
      color: AppColors.white,
      fontWeight: FontWeight.w500);
  static TextStyle discription = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 16,
    fontWeight: FontWeight.w400,
    color: AppColors.black,
  );
  static TextStyle home = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 20,
    fontWeight: FontWeight.w400,
    color: AppColors.primary,
  );
  static TextStyle current = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 18,
    fontWeight: FontWeight.w500,
    color: AppColors.primary,
  );
  static TextStyle title_home = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 11,
    fontWeight: FontWeight.w600,
    color: AppColors.grey,
  );
  static TextStyle discription_home = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: AppColors.primary,
  );
  static TextStyle boder = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 13,
    fontWeight: FontWeight.w600,
    color: AppColors.orange,
  );
  static TextStyle bottomappbar = const TextStyle(
    fontFamily: FontFamily.inter,
    fontSize: 12,
    fontWeight: FontWeight.w500,
    color: AppColors.grey,
  );
}
