import 'package:flutter/material.dart';

/// define colors
class AppColors {
  static const Color primaryColor = Color(0xff041967);

  static const Color aqua = Color(0xff00AB97);
  static const Color grey = Color(0xff3B3B3B);
  static const Color lightgrey = Color(0xffC4C4C4);
  static const Color primary = Color(0xff041967);
  static const Color black = Color(0xff000000);
  static const Color blue = Color(0xff008DFF);
  static const Color white = Color(0xffFFFFFF);
  static const Color border = Color(0xffD6DEED);
  static const Color yellow = Color(0xffFFDE4A);
  static const Color orange = Color(0xffCA8E03);
  static const Color backgroundorange = Color(0xffFFFFFF);
  static const Color background = Color(0xffE1E9F1);
  static const Color navigativeappbar = Color(0xff008DFF);
  static const Color frame = Color(0xffDAE7F3);
  //static const Color lightgrey = Color(0xff8C8C8C);
  static const Color red = Color(0xffEF3900);
  static const Color frame_failed = Color(0xffC5CED7);
}
