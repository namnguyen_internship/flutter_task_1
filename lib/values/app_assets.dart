/// define path of assets(image)

class AppAssets {
  static const String imagepath = 'assets/images/';
  static const String frame = '${imagepath}Frame.svg';
  static const String pic_1 = '${imagepath}Pic_1.svg';
  static const String pic_2 = '${imagepath}Pic_2.svg';
  static const String pic_3 = '${imagepath}Pic_3.svg';
  static const String skip = '${imagepath}skip.svg';
  static const String picture = '${imagepath}pictures.svg';
  static const String phone = '${imagepath}phone.svg';
  static const String down = '${imagepath}down.svg';
  static const String back = '${imagepath}back.svg';
  static const String menu = '${imagepath}menu.svg';
  static const String home = '${imagepath}home.svg';
  static const String next = '${imagepath}next.svg';
  static const String oders = '${imagepath}oders.svg';
  static const String messages = '${imagepath}messages.svg';
  static const String notifications = '${imagepath}notifications.svg';
  static const String profile = '${imagepath}profile.svg';
  static const String filter = '${imagepath}filter.svg';
  static const String contact = '${imagepath}contact.svg';
  static const String file_text = '${imagepath}file_text.svg';
  static const String log_out = '${imagepath}log_out.svg';
  static const String notification = '${imagepath}notification.svg';
  static const String avatar = '${imagepath}avatar.png';
  static const String edit = '${imagepath}edit.svg';
}
