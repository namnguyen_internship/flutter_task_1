import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app1/pages/models/verificationInfo.dart';
import 'package:http/http.dart' as http;

Future<LoginModel?> submitDataOTP(String phone_number, String country_code,
    String otp_code, BuildContext context) async {
  final endPoint = "rxdn-hub-qa.opusasia.io";
  final client = http.Client();
  var headers = {'Content-Type': 'application/json'};
  final uri = Uri.https(endPoint, 'api/v1/recipients/login');
  var request = http.Request('POST', uri);
  request.body = json.encode({
    "recipient": {
      "phone_number": phone_number,
      "country_code": country_code,
      "otp_code": otp_code,
    }
  });
  request.headers.addAll(headers);
  var streamedResponse = await request.send();
  var response = await http.Response.fromStream(streamedResponse);
  if (response.statusCode == 200) {
    final String responseString = response.body;
    return loginModelFromJson(responseString);
  } else {
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text("verification code is not correct!")));
    return null;
  }
}
