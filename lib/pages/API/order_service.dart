import 'package:flutter_app1/pages/models/orderinfo.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class API_manager {
  String accesstoken;
  API_manager({required this.accesstoken});
  final endPoint = "rxdn-hub-qa.opusasia.io";
  final client = http.Client();
  Future<List<OrderModel>> getOrderModel() async {
    final queryParameters = {'page': '1', 'limit': '1'};
    final uri =
        Uri.https(endPoint, '/api/v1/recipients/orders', queryParameters);
    final response = await client.get(uri, headers: {
      'AUTH_TOKEN': accesstoken,
      'Authorization': 'Bearer ${accesstoken}',
    });
    Map<String, dynamic> json = jsonDecode(response.body);
    List<dynamic> body = json['data'];
    List<OrderModel> data =
        body.map((dynamic item) => OrderModel.fromJson(item)).toList();
    return data;
  }
}
