// import 'package:flutter/material.dart';
// import 'package:flutter/src/foundation/key.dart';
// import 'package:flutter/src/widgets/framework.dart';
// import 'package:flutter_app1/values/app_assets.dart';
// import 'package:flutter_app1/values/app_colors.dart';
// import 'package:flutter_app1/values/app_styles.dart';
// import 'package:flutter_svg/flutter_svg.dart';

// class accesspage extends StatefulWidget {
//   const accesspage({Key? key}) : super(key: key);

//   @override
//   State<accesspage> createState() => _accesspageState();
// }

// class _accesspageState extends State<accesspage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: AppColors.primary,
//         title: Container(
//           margin: const EdgeInsets.only(top: 40, bottom: 20, left: 80),
//           child: Text(
//             'Verification',
//             style: AppStyles.title.copyWith(color: AppColors.white),
//           ),
//         ),
//         leading: (Container(
//           child: IconButton(
//             padding: EdgeInsets.only(top: 20, right: 15),
//             icon: SvgPicture.asset(
//               AppAssets.back,
//               color: AppColors.white,
//             ),
//             onPressed: () {
//               Navigator.pop(context);
//             },
//           ),
//         )),
//       ),
//       body: Container(
//           height: 120,
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Text(
//                 "verification",
//                 style: AppStyles.discription.copyWith(fontSize: 50),
//               ),
//             ],
//           )),
//     );
//   }
// }
