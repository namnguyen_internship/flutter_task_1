class ListValues {
  String imagepath;
  ListValues({required this.imagepath});
}

var listofvalue = [
  ListValues(imagepath: 'assets/images/Pic_1.svg'),
  ListValues(imagepath: 'assets/images/Pic_2.svg'),
  ListValues(imagepath: 'assets/images/Pic_3.svg'),
];

class ListText {
  String text;
  ListText({required this.text});
}

var listoftext = [
  ListText(text: 'Delivery Scheduled'),
  ListText(text: 'Get notified'),
  ListText(text: 'Track'),
];

class ListDiscription {
  String text;
  ListDiscription({required this.text});
}

var listofdiscription = [
  ListText(
      text:
          'Message thi de "Your pharmacy schedules your medication delivery"'),
  ListText(text: 'You get notified when your medication is en route'),
  ListText(text: 'Track your order and view order history'),
];
