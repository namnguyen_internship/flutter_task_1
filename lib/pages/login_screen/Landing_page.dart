import 'package:flutter/material.dart';
import 'package:flutter_app1/pages/login_screen/home_page.dart';
import 'package:flutter_app1/values/app_assets.dart';
import 'package:flutter_app1/values/app_colors.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:async';

class LandingPage extends StatefulWidget {
  const LandingPage({Key? key}) : super(key: key);

  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  @override
  void initState() {
    super.initState();
    Timer(
        const Duration(seconds: 1),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => const MyHome())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primaryColor,
      body: Container(
        height: 760,
        width: 414,
        child: Column(
          children: [
            Container(
                padding: EdgeInsets.only(
                    top: 314, bottom: 314, left: 53, right: 53.21),
                child: SvgPicture.asset(
                  AppAssets.frame,
                  height: 131.11,
                  width: 387.79,
                ))
          ],
        ),
      ),
    );
  }
}
