import 'package:flutter_app1/pages/home_screen/main_page.dart';
import 'package:flutter_app1/pages/login_screen/login_page.dart';
import 'package:flutter_app1/pages/models/verificationInfo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app1/pages/API/login_service.dart';
import 'package:flutter_app1/values/app_assets.dart';
import 'package:flutter_app1/values/app_colors.dart';
import 'package:flutter_app1/values/app_styles.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class VerificationPage extends StatefulWidget {
  String areaNumber;
  String phone;

  VerificationPage({required this.phone, required this.areaNumber});
  @override
  State<VerificationPage> createState() => _VerificationPageState();
}

class _VerificationPageState extends State<VerificationPage> {
  final TextEditingController verificationcontroler = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primary,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: AppColors.primary,
              height: 210,
              width: 414,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(AppAssets.picture),
                ],
              ),
            ),
            Container(
              height: 549,
              width: 414,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(20.0),
                  topRight: const Radius.circular(20.0),
                ),
              ),
              child: Column(
                children: [
                  Container(
                      margin: EdgeInsets.only(top: 24, left: 16),
                      height: 24,
                      width: 414,
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 16),
                            child: InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: SvgPicture.asset(
                                  AppAssets.back,
                                )),
                          ),
                          Text(
                            'Back',
                            style: AppStyles.title
                                .copyWith(fontWeight: FontWeight.w400),
                          ),
                        ],
                      )),
                  Container(
                    height: 370,
                    width: 311,
                    margin: EdgeInsets.only(
                        top: 34, left: 47, right: 47, bottom: 20),
                    child: Column(
                      children: [
                        Container(
                          height: 24,
                          margin: EdgeInsets.only(bottom: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('OTP Verification', style: AppStyles.title)
                            ],
                          ),
                        ),
                        Container(
                          height: 56,
                          width: 280,
                          margin: EdgeInsets.only(bottom: 20),
                          child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                  text:
                                      'A verification codes has been sent to ',
                                  style: AppStyles.discription,
                                  children: [
                                    TextSpan(
                                        text: '(${widget.areaNumber})'
                                            ' '
                                            '${widget.phone.substring(3, 6)}'
                                            ' - '
                                            '${widget.phone.substring(7, widget.phone.length)}',
                                        style: AppStyles.discription.copyWith(
                                            color: AppColors.blue,
                                            fontWeight: FontWeight.w600)),
                                  ])),
                        ),
                        Container(
                          height: 53,
                          child: PinCodeTextField(
                            controller: verificationcontroler,
                            length: 6,
                            textStyle: AppStyles.title,
                            cursorColor: AppColors.primary,
                            keyboardType: TextInputType.number,
                            appContext: context,
                            pinTheme: PinTheme(
                                activeColor: AppColors.border,
                                selectedColor: AppColors.border,
                                inactiveColor: AppColors.border),
                            onChanged: (value) {
                              //print(value);
                            },
                            hintCharacter: '-',
                            hintStyle:
                                AppStyles.title.copyWith(color: AppColors.grey),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 36, bottom: 58),
                          width: 311,
                          height: 42,
                          child: ElevatedButton(
                            child: Text(
                              'VERIFY & CONTINUE',
                              style: AppStyles.button,
                            ),
                            onPressed: () async {
                              final LoginModel? login = await submitDataOTP(
                                  widget.phone,
                                  widget.areaNumber,
                                  verificationcontroler.text,
                                  context);
                              if (login!.data.accessToken != null) {
                                print(login.data.accessToken);
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => MainPage(
                                            accesstoken:
                                                login.data.accessToken)),
                                    (route) => false);
                              }
                            },
                            style: ElevatedButton.styleFrom(
                              primary: AppColors.primary,
                              onPrimary: AppColors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          height: 28,
                          child: Text(
                            'Send agian OTP',
                            style: AppStyles.discription,
                          ),
                        ),
                        Container(
                          height: 24,
                          child: TweenAnimationBuilder(
                              tween: Tween(
                                  begin: Duration(minutes: 1),
                                  end: Duration.zero),
                              duration: Duration(minutes: 1),
                              builder: (BuildContext context, Duration value,
                                  Widget? child) {
                                final seconds = value.inSeconds % 60;
                                return Text(
                                  '00:${seconds}',
                                  style: AppStyles.title
                                      .copyWith(color: AppColors.blue),
                                );
                              }),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 50,
                    child: TextButton(
                        onPressed: () async {
                          void data = await submitData(
                              widget.phone, widget.areaNumber, context);
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: ((context) => VerificationPage(
                                      phone: widget.phone,
                                      areaNumber: widget.areaNumber))));
                        },
                        child: Text(
                          'RESEND CODE',
                          style: AppStyles.discription.copyWith(
                              fontWeight: FontWeight.w500,
                              color: AppColors.blue,
                              decoration: TextDecoration.underline),
                        )),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
