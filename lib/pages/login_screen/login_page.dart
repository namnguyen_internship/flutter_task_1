import 'dart:async';
import 'package:flutter_app1/pages/API/login_service.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_app1/pages/login_screen/verification_page.dart';
import 'package:flutter_app1/values/app_assets.dart';
import 'package:flutter_app1/values/app_colors.dart';
import 'package:flutter_app1/values/app_styles.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class PhonePage extends StatefulWidget {
  const PhonePage({Key? key}) : super(key: key);
  @override
  State<PhonePage> createState() => _PhonePageState();
}

Future<void> submitData(
    String phone_number, String country_code, BuildContext context) async {
  var headers = {'Content-Type': 'application/json'};
  var request = http.Request('POST',
      Uri.parse('https://rxdn-hub-qa.opusasia.io/api/v1/recipients/send_otp'));
  request.body = json.encode({
    "recipient": {"phone_number": phone_number, "country_code": country_code}
  });
  request.headers.addAll(headers);

  http.StreamedResponse response = await request.send();

  if (response.statusCode == 200) {
    Navigator.of(context).push(
      MaterialPageRoute(
          builder: (c) => VerificationPage(
              areaNumber: country_code.substring(1, country_code.length - 1),
              phone: phone_number)),
    );
  } else {
    CircularProgressIndicator();
  }
}

class _PhonePageState extends State<PhonePage> {
  final formKey = GlobalKey<FormState>();
  String errorTextvalue = '';
  final maskFormatter = MaskTextInputFormatter(mask: '   ### #######');
  List<String> items = [
    '(+1)',
    '(+84)',
    '(+33)',
    '(+358)',
    '(+46)',
    '(+44)',
    '(+7)',
    '(+63)'
  ];
  String _selectedItem = '(+1)';
  final TextEditingController numberController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primary,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: AppColors.primary,
              height: 210,
              width: 414,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(AppAssets.picture),
                ],
              ),
            ),
            Container(
              height: 549,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(20.0),
                  topRight: const Radius.circular(20.0),
                ),
              ),
              child: Container(
                height: 257,
                width: 311,
                margin: EdgeInsets.only(top: 50, left: 47, right: 47),
                child: Column(
                  children: [
                    Container(
                      height: 24,
                      margin: EdgeInsets.only(bottom: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('Enter Your Phone Number',
                              style: AppStyles.title.copyWith(fontSize: 20))
                        ],
                      ),
                    ),
                    Container(
                      height: 56,
                      margin: EdgeInsets.only(bottom: 44),
                      child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              text: 'We will send you the ',
                              style: AppStyles.discription,
                              children: [
                                TextSpan(
                                    text: '6 digit',
                                    style: AppStyles.discription.copyWith(
                                        color: AppColors.blue,
                                        fontWeight: FontWeight.w600)),
                                TextSpan(
                                    text: ' verification code',
                                    style: AppStyles.discription),
                              ])),
                    ),
                    Container(
                        height: 49,
                        width: 311,
                        child: Form(
                          key: formKey,
                          child: TextFormField(
                            controller: numberController,
                            inputFormatters: [maskFormatter],
                            keyboardType: TextInputType.number,
                            style: AppStyles.button.copyWith(
                                color: AppColors.primary, fontSize: 16),
                            cursorColor: AppColors.primary,
                            validator: ((value) {
                              if (value!.length >= 11) {
                                return null;
                              } else {
                                return 'please enter at least 7 number';
                              }
                            }),
                            onSaved: (value) =>
                                setState(() => errorTextvalue = value!),
                            decoration: InputDecoration(
                              prefix: Container(
                                height: 20,
                                decoration: BoxDecoration(
                                  border: BorderDirectional(
                                      end: BorderSide(
                                    color: AppColors.grey,
                                  )),
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: DropdownButton<String>(
                                    icon: SvgPicture.asset(AppAssets.down),
                                    value: _selectedItem,
                                    items: items
                                        .map((item) => DropdownMenuItem<String>(
                                              value: item,
                                              child: Text(
                                                item,
                                                style: AppStyles.button
                                                    .copyWith(
                                                        color:
                                                            AppColors.primary,
                                                        fontSize: 16),
                                              ),
                                            ))
                                        .toList(),
                                    onChanged: (item) =>
                                        setState(() => _selectedItem = item!),
                                  ),
                                ),
                              ),
                              prefixIcon: Container(
                                margin: EdgeInsets.only(
                                    right: 25, bottom: 15, top: 15),
                                child: SvgPicture.asset(AppAssets.phone),
                              ),
                              labelText: 'Your Phone Number',
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.never,
                            ),
                          ),
                        )),
                    Container(
                      margin: EdgeInsets.only(top: 36),
                      width: 311,
                      height: 42,
                      child: ElevatedButton(
                        child: Text(
                          'NEXT',
                          style: AppStyles.button,
                        ),
                        onPressed: () async {
                          final isValid = formKey.currentState!.validate();
                          if (isValid) {
                            formKey.currentState!.save();
                            void data = await submitData(
                                numberController.text, _selectedItem, context);
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          primary: AppColors.primary,
                          onPrimary: AppColors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
