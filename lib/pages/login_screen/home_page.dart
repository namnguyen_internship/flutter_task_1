import 'package:flutter/material.dart';
import 'package:flutter_app1/pages/login_screen/listvalues.dart';
import 'package:flutter_app1/pages/login_screen/login_page.dart';
import 'package:flutter_app1/values/app_colors.dart';
import 'package:flutter_app1/values/app_styles.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MyHome extends StatefulWidget {
  const MyHome({Key? key}) : super(key: key);

  @override
  State<MyHome> createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  int _currentIndex = 0;
  PageController _pageController = PageController();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: double.infinity,
        child: PageView.builder(
            controller: _pageController,
            onPageChanged: (index) {
              setState(() {
                _currentIndex = index;
              });
            },
            itemCount: 3,
            itemBuilder: (context, index) {
              return Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 180),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          listofvalue[index].imagepath,
                          height: 204.63,
                          width: 348,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          height: 24,
                          width: 183,
                          margin: EdgeInsets.only(top: 50, bottom: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(listoftext[index].text,
                                  style: AppStyles.namepage),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 32),
                          height: 44,
                          width: 310,
                          child: Text(
                            listofdiscription[index].text,
                            style: AppStyles.order.copyWith(fontSize: 14),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Container(
                            height: 5,
                            margin: EdgeInsets.only(left: 180, bottom: 64),
                            child: ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                scrollDirection: Axis.horizontal,
                                itemCount: 3,
                                itemBuilder: (context, index) {
                                  return buildIndicator(
                                      index == _currentIndex, size);
                                })),
                        Container(
                          width: 140,
                          height: 42,
                          margin: EdgeInsets.symmetric(horizontal: 85),
                          child: ElevatedButton(
                            child: Text(
                              'SKIP',
                              style: AppStyles.button,
                            ),
                            onPressed: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(builder: (_) => PhonePage()),
                              );
                            },
                            style: ElevatedButton.styleFrom(
                              primary: AppColors.primaryColor,
                              onPrimary: AppColors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(25.0),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
            }),
      ),
    );
  }

  Widget buildIndicator(bool isActive, Size size) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 2),
      width: isActive ? 5 : 5,
      decoration: BoxDecoration(
        color: isActive ? AppColors.primaryColor : AppColors.lightgrey,
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }
}
