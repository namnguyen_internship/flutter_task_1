import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app1/pages/API/order_service.dart';
import 'package:flutter_app1/pages/models/orderinfo.dart';
import 'package:flutter_app1/values/app_assets.dart';
import 'package:flutter_app1/values/app_colors.dart';
import 'package:flutter_app1/values/app_styles.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

class PageOrder extends StatefulWidget {
  String accesstoken;
  PageOrder({Key? key, required this.accesstoken}) : super(key: key);

  @override
  State<PageOrder> createState() => _PageOrderState();
}

class _PageOrderState extends State<PageOrder> {
  @override
  Widget build(BuildContext context) {
    API_manager client = API_manager(accesstoken: widget.accesstoken);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.yellow,
        title: Container(
          margin: EdgeInsets.only(top: 40, bottom: 20),
          child: Text(
            'ORDERS',
            style: AppStyles.home,
          ),
        ),
        centerTitle: true,
        actions: [
          Container(
            child: IconButton(
              padding: EdgeInsets.only(top: 20),
              icon: SvgPicture.asset(AppAssets.filter),
              onPressed: () {
                print(widget.accesstoken);
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          DefaultTabController(
              length: 2, // length of tabs
              initialIndex: 0,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 16),
                      child: TabBar(
                        labelColor: AppColors.primary,
                        unselectedLabelColor: AppColors.lightgrey,
                        indicatorColor: AppColors.primary,
                        tabs: [
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  padding: EdgeInsets.only(top: 20, bottom: 11),
                                  child: Text(
                                    'UPCOMING',
                                    style: TextStyle(
                                        fontFamily: FontFamily.inter,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  padding: EdgeInsets.only(top: 20, bottom: 11),
                                  child: Text(
                                    'HISTORY',
                                    style: TextStyle(
                                        fontFamily: FontFamily.inter,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.symmetric(horizontal: 16),
                        height: 800, //height of TabBarView
                        decoration: BoxDecoration(
                            border: Border(
                                top: BorderSide(
                                    color: AppColors.border, width: 1))),
                        child: Container(
                          margin: EdgeInsets.only(top: 16),
                          child: TabBarView(children: <Widget>[
                            Container(),
                            Container(
                              child: FutureBuilder(
                                future: client.getOrderModel(),
                                builder: (BuildContext context,
                                    AsyncSnapshot<List<OrderModel>> snapshot) {
                                  if (snapshot.hasData) {
                                    List<OrderModel>? OrderModels =
                                        snapshot.data;
                                    return ListView.builder(
                                        itemCount: OrderModels?.length,
                                        itemBuilder: (context, index) =>
                                            OrderModels![index].stageName ==
                                                    "Order Failed"
                                                ? Failed_Items(
                                                    OrderModels, index)
                                                : Container());
                                  }
                                  return Center(
                                    child: CircularProgressIndicator(),
                                  );
                                },
                              ),
                            ),
                          ]),
                        ))
                  ])),
        ]),
      ),
    );
  }
}

Widget Failed_Items(List<OrderModel>? OrderModels, index) {
  String timeFormat(int? time,
      {bool day = false, String format = 'dd MMM yyyy'}) {
    if (time != null) {
      return DateFormat(day ? 'EEE, MM/dd/yyyy hh:mm a' : format).format(
        DateTime.fromMillisecondsSinceEpoch(time),
      );
    }
    return '-';
  }

  return Container(
    margin: EdgeInsets.only(top: 15),
    child: DottedBorder(
        color: AppColors.frame_failed,
        strokeWidth: 3,
        dashPattern: [6, 6],
        borderType: BorderType.RRect,
        radius: Radius.circular(20),
        child: Container(
          height: 167,
          width: 382,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16, top: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Container(
                              child: Text('Order Placed',
                                  style: AppStyles.title_home),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              child: Text(
                                  timeFormat(
                                      OrderModels![index].pickupStartTime),
                                  style: AppStyles.discription
                                      .copyWith(fontSize: 13)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 16, top: 16),
                    child: Column(
                      children: [
                        Container(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 3.5),
                            child: Text(
                              'Failed',
                              style: AppStyles.boder
                                  .copyWith(color: AppColors.red, fontSize: 13),
                            ),
                          ),
                          height: 31,
                          width: 62,
                          decoration: BoxDecoration(
                            color: AppColors.backgroundorange,
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(
                              color: AppColors.red,
                              width: 3,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(
                    color: AppColors.border,
                    width: 1,
                  ),
                ),
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              child: Text(
                                'Pharmacy Name',
                                style: AppStyles.title_home,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              child: Text(
                                OrderModels[index].store.name,
                                style: AppStyles.discription
                                    .copyWith(fontSize: 18),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(bottom: 2, top: 12),
                              child: Text(
                                'Medication Name',
                                style: AppStyles.title_home,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              child: Text(
                                OrderModels[index]
                                    .orderDetails[0]
                                    .pharmaceuticalName,
                                style: AppStyles.discription
                                    .copyWith(fontSize: 16),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          decoration: BoxDecoration(
              color: AppColors.background,
              borderRadius: BorderRadius.circular(16)),
        )),
  );
}
