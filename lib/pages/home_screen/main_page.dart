import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app1/pages/home_screen/home_page.dart';
import 'package:flutter_app1/pages/home_screen/order_page.dart';
import 'package:flutter_app1/values/app_assets.dart';
import 'package:flutter_app1/values/app_colors.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MainPage extends StatefulWidget {
  String accesstoken;
  MainPage({Key? key, required this.accesstoken}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> pageList = <Widget>[
      PageHome(),
      PageOrder(accesstoken: widget.accesstoken),
    ];
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: pageList[selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 13,
        items: [
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppAssets.home,
              color: selectedIndex == 0
                  ? AppColors.navigativeappbar
                  : AppColors.grey,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppAssets.oders,
              color: selectedIndex == 1
                  ? AppColors.navigativeappbar
                  : AppColors.grey,
            ),
            label: 'Oders',
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              AppAssets.messages,
              color: selectedIndex == 2
                  ? AppColors.navigativeappbar
                  : AppColors.grey,
            ),
            label: 'Messages',
          ),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                AppAssets.notifications,
                color: selectedIndex == 3
                    ? AppColors.navigativeappbar
                    : AppColors.grey,
              ),
              label: 'Notifications'),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                AppAssets.profile,
                color: selectedIndex == 4
                    ? AppColors.navigativeappbar
                    : AppColors.grey,
              ),
              label: 'Profile')
        ],
        currentIndex: selectedIndex,
        selectedItemColor: AppColors.navigativeappbar,
        onTap: _onItemTapped,
      ),
    );
  }
}
