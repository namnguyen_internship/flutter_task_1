import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_switch/flutter_switch.dart';

import '../../values/app_assets.dart';
import '../../values/app_colors.dart';
import '../../values/app_styles.dart';

class NavigationDrawer extends StatefulWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  State<NavigationDrawer> createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {
  bool isToggled = false;
  @override
  Widget build(BuildContext context) => Drawer(
        child: Column(
          children: [
            Container(
              height: 615,
              width: 232,
              margin: EdgeInsets.only(left: 24, right: 24, top: 12, bottom: 16),
              child: Column(
                children: <Widget>[
                  buildHeader(context),
                  buildMenuItems(context),
                ],
              ),
            ),
            Container(
                height: 48,
                width: 232,
                margin: EdgeInsets.only(left: 24, right: 24, bottom: 12),
                child: buildMoreInformation(context)),
          ],
        ),
      );
  Widget buildHeader(BuildContext context) => Container(
        height: 136,
        width: 232,
        margin: EdgeInsets.only(top: 48),
        child: Column(
          children: [
            Container(
              width: 232,
              height: 76,
              child: Container(
                margin: EdgeInsets.only(top: 8, bottom: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(AppAssets.avatar),
                  ],
                ),
              ),
            ),
            Container(
              height: 20,
              width: 232,
              margin: EdgeInsets.only(top: 8, bottom: 32),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 18),
                    child: Column(
                      children: [
                        Text(
                          'Jona William',
                          style: AppStyles.discription_home,
                        )
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      SvgPicture.asset(AppAssets.edit),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      );
  Widget buildMenuItems(BuildContext context) => Column(children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(
              color: AppColors.border,
              width: 1,
            ),
          ),
        ),
        Container(
          height: 60,
          width: 232,
          margin: EdgeInsets.only(top: 16),
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 12),
                        child: Column(
                          children: [
                            SvgPicture.asset(
                              AppAssets.notification,
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          Text('Notification',
                              style: TextStyle(
                                  fontFamily: FontFamily.inter,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.black)),
                        ],
                      ),
                    ],
                  ),
                ),
                Column(
                  children: [
                    FlutterSwitch(
                      height: 15.17,
                      width: 23.83,
                      toggleSize: 5,
                      borderRadius: 10,
                      padding: 3,
                      activeSwitchBorder:
                          Border.all(color: AppColors.primary, width: 1.5),
                      inactiveSwitchBorder:
                          Border.all(color: AppColors.primary, width: 1.5),
                      activeColor: AppColors.backgroundorange,
                      inactiveColor: AppColors.backgroundorange,
                      toggleColor: AppColors.primary,
                      value: isToggled,
                      onToggle: (value) {
                        setState(() {
                          isToggled = value;
                        });
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Container(
          height: 60,
          width: 232,
          child: InkWell(
            onTap: () {},
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.only(right: 12),
                  child: SvgPicture.asset(
                    AppAssets.contact,
                  ),
                ),
                Container(
                  child: Text('Terms & Conditions',
                      style: TextStyle(
                          fontFamily: FontFamily.inter,
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: AppColors.black)),
                ),
              ],
            ),
          ),
        ),
        Container(
          height: 60,
          width: 232,
          child: InkWell(
            onTap: () {},
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.only(right: 12),
                  child: SvgPicture.asset(
                    AppAssets.file_text,
                  ),
                ),
                Container(
                  child: Text('Privacy Policy',
                      style: TextStyle(
                          fontFamily: FontFamily.inter,
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: AppColors.black)),
                ),
              ],
            ),
          ),
        ),
        Container(
          height: 60,
          width: 232,
          child: InkWell(
            onTap: () {},
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.only(right: 12),
                  child: SvgPicture.asset(
                    AppAssets.log_out,
                  ),
                ),
                Container(
                  child: Text('Log  Out',
                      style: TextStyle(
                          fontFamily: FontFamily.inter,
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: AppColors.red)),
                ),
              ],
            ),
          ),
        ),
      ]);
  Widget buildMoreInformation(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 16),
            child: Text('Version',
                style: AppStyles.boder.copyWith(color: AppColors.black)),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 16),
            child: Text('1.0.5 (2)',
                style: AppStyles.boder.copyWith(color: AppColors.black)),
          ),
        ],
      );
}
