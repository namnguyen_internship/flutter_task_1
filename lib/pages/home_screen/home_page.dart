import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_app1/pages/home_screen/Drawer_page.dart';
import 'package:flutter_app1/pages/home_screen/upcoming_page.dart';
import 'package:flutter_app1/values/app_assets.dart';
import 'package:flutter_app1/values/app_colors.dart';
import 'package:flutter_app1/values/app_styles.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PageHome extends StatefulWidget {
  const PageHome({Key? key}) : super(key: key);

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: AppColors.yellow,
        title: Container(
          margin: EdgeInsets.only(top: 40, bottom: 20),
          child: Text(
            'HOME',
            style: AppStyles.home,
          ),
        ),
        centerTitle: true,
        leading: (Container(
          child: IconButton(
            padding: EdgeInsets.only(top: 20, right: 15),
            icon: SvgPicture.asset(AppAssets.menu),
            onPressed: () {
              _scaffoldKey.currentState?.openDrawer();
            },
          ),
        )),
      ),
      drawer: NavigationDrawer(),
      body: SingleChildScrollView(
        child: Column(children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Row(
                children: [Text('CURRENT ORDERS', style: AppStyles.current)]),
          ),
          Container(
            height: 262,
            width: 382,
            margin: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 16, top: 16),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 100),
                                child: Text('Order Placed',
                                    style: AppStyles.title_home),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 5),
                                child: Text('Mon, 16 Oct 2022 8:00 AM',
                                    style: AppStyles.discription_home
                                        .copyWith(fontSize: 13)),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 16, top: 16),
                      child: Column(
                        children: [
                          Container(
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 3.5),
                              child: Text(
                                'Out For Delivery',
                                style: AppStyles.boder,
                              ),
                            ),
                            height: 31,
                            width: 126,
                            decoration: BoxDecoration(
                              color: AppColors.backgroundorange,
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                color: AppColors.orange,
                                width: 3,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(
                      color: AppColors.border,
                      width: 1,
                    ),
                  ),
                ),
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 16),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(right: 162, bottom: 2),
                                child: Text(
                                  'Delivery Address',
                                  style: AppStyles.title_home,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                'The Downtown Dispensary Clinic',
                                style: AppStyles.discription_home,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                    right: 128, bottom: 2, top: 12),
                                child: Text(
                                  'Estimated For Delivery',
                                  style: AppStyles.title_home,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(right: 29),
                                child: Text(
                                  'Mon, 10/16/2022 4:30 PM',
                                  style: AppStyles.discription_home
                                      .copyWith(fontSize: 18),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                    right: 156, bottom: 2, top: 12),
                                child: Text(
                                  'Medication Name',
                                  style: AppStyles.title_home,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(right: 134),
                                child: Text(
                                  'Eating Disorder',
                                  style: AppStyles.discription_home,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                    right: 156, bottom: 2, top: 12),
                                child: Text(
                                  'Delivery Address',
                                  style: AppStyles.title_home,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(right: 90),
                                child: Text(
                                  '123 Brockton Avenue',
                                  style: AppStyles.discription_home,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
            decoration: BoxDecoration(
              color: AppColors.background,
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                color: AppColors.frame,
                width: 3,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 32),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Text('UPCOMING ORDERS', style: AppStyles.current),
                  ],
                ),
                Column(
                  children: [
                    Container(
                      height: 20,
                      child: IconButton(
                        padding: EdgeInsets.only(left: 20),
                        icon: SvgPicture.asset(AppAssets.next),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (_) => PageUpcoming()));
                        },
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          Container(
            height: 170,
            width: 382,
            margin: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 16, top: 16),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 100),
                                child: Text('Order Placed',
                                    style: AppStyles.title_home),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 5),
                                child: Text('Mon, 16 Oct 2022 8:00 AM',
                                    style: AppStyles.discription_home
                                        .copyWith(fontSize: 13)),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 16, top: 16),
                      child: Column(
                        children: [
                          Container(
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 3.5),
                              child: Text(
                                'Out For Delivery',
                                style: AppStyles.boder,
                              ),
                            ),
                            height: 31,
                            width: 126,
                            decoration: BoxDecoration(
                              color: AppColors.backgroundorange,
                              borderRadius: BorderRadius.circular(30),
                              border: Border.all(
                                color: AppColors.orange,
                                width: 3,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(
                      color: AppColors.border,
                      width: 1,
                    ),
                  ),
                ),
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 16),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(right: 206, bottom: 1),
                                child: Text(
                                  'Estimated For Delivery',
                                  style: AppStyles.title_home,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(right: 200),
                                child: Text(
                                  'Friday, 04:30 p',
                                  style: AppStyles.discription_home
                                      .copyWith(fontSize: 18),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                    right: 230, bottom: 2, top: 12),
                                child: Text('Delivery Address',
                                    style: AppStyles.title_home),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(right: 50),
                                child: Text(
                                  '777 Brockton Avenue, Abington MA',
                                  style: AppStyles.discription_home,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
            decoration: BoxDecoration(
              color: AppColors.background,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(
                color: AppColors.frame,
                width: 3,
              ),
            ),
          ),
          Container(
            height: 170,
            width: 382,
            margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 16, top: 16),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 100),
                                child: Text('Order Placed',
                                    style: AppStyles.title_home),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 5),
                                child: Text('Mon, 16 Oct 2022 8:00 AM',
                                    style: AppStyles.discription_home
                                        .copyWith(fontSize: 13)),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(right: 16, top: 16),
                      child: Column(
                        children: [
                          Container(
                            // padding: EdgeInsets.only(right: 16),
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8, vertical: 3.5),
                              child: Text(
                                'Order Placed',
                                style: AppStyles.boder.copyWith(
                                    color: AppColors.primary, fontSize: 13),
                              ),
                            ),
                            height: 31,
                            width: 106,
                            decoration: BoxDecoration(
                              color: AppColors.backgroundorange,
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(
                                color: AppColors.primary,
                                width: 3,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    border: Border.all(
                      color: AppColors.border,
                      width: 1,
                    ),
                  ),
                ),
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 16),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(right: 145, bottom: 2),
                                child: Text(
                                  'Estimated For Delivery',
                                  style: AppStyles.title_home,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(right: 47),
                                child: Text(
                                  'Mon, 10/16/2022 4:30 PM',
                                  style: AppStyles.discription_home
                                      .copyWith(fontSize: 18),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                    right: 175, bottom: 2, top: 12),
                                child: Text(
                                  'Delivery Address',
                                  style: AppStyles.title_home,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                child: Text(
                                  '777 Brockton Avenue, Abington MA',
                                  style: AppStyles.discription_home
                                      .copyWith(fontSize: 16),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
            decoration: BoxDecoration(
              color: AppColors.background,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(
                color: AppColors.frame,
                width: 3,
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
