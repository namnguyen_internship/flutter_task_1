import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_app1/values/app_assets.dart';
import 'package:flutter_app1/values/app_colors.dart';
import 'package:flutter_app1/values/app_styles.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PageUpcoming extends StatefulWidget {
  const PageUpcoming({Key? key}) : super(key: key);

  @override
  State<PageUpcoming> createState() => _PageUpcomingState();
}

class _PageUpcomingState extends State<PageUpcoming> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.yellow,
        title: Container(
          margin: EdgeInsets.only(top: 40, bottom: 20),
          child: Text(
            'UPCOMING ORDERS',
            style: AppStyles.home,
          ),
        ),
        centerTitle: true,
        leading: (Container(
          child: IconButton(
            padding: EdgeInsets.only(top: 20, right: 15),
            icon: SvgPicture.asset(AppAssets.back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        )),
        actions: [
          Container(
            child: IconButton(
              padding: EdgeInsets.only(top: 20),
              icon: SvgPicture.asset(AppAssets.filter),
              onPressed: () {
                print('hello');
              },
            ),
          ),
        ],
      ),
      body: Column(children: [
        Container(
          height: 176,
          width: 382,
          margin: EdgeInsets.only(top: 14, bottom: 12, left: 16, right: 16),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16, top: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 100),
                              child: Text('Order Placed',
                                  style: AppStyles.title_home),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 5),
                              child: Text('Mon, 16 Oct 2022 8:00 AM',
                                  style: AppStyles.discription_home
                                      .copyWith(fontSize: 13)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 16, top: 16),
                    child: Column(
                      children: [
                        Container(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 3.5),
                            child: Text(
                              'Order Placed',
                              style: AppStyles.boder.copyWith(
                                  color: AppColors.primary, fontSize: 13),
                            ),
                          ),
                          height: 31,
                          width: 106,
                          decoration: BoxDecoration(
                            color: AppColors.backgroundorange,
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(
                              color: AppColors.primary,
                              width: 3,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(
                    color: AppColors.border,
                    width: 1,
                  ),
                ),
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 145, bottom: 2),
                              child: Text(
                                'Estimated For Delivery',
                                style: AppStyles.title_home,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 47),
                              child: Text(
                                'Mon, 10/16/2022 4:30 PM',
                                style: AppStyles.discription_home
                                    .copyWith(fontSize: 18),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  right: 180, bottom: 2, top: 12),
                              child: Text(
                                'Pharmacy Name',
                                style: AppStyles.title_home,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 148),
                              child: Text(
                                'Roosevelt Clinic',
                                style: AppStyles.discription_home
                                    .copyWith(fontSize: 16),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          decoration: BoxDecoration(
            color: AppColors.background,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(
              color: AppColors.frame,
              width: 3,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 12, left: 16, right: 16),
          height: 170,
          width: 382,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16, top: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 100),
                              child: Text('Order Placed',
                                  style: AppStyles.title_home),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 5),
                              child: Text('Mon, 16 Oct 2022 8:00 AM',
                                  style: AppStyles.discription_home
                                      .copyWith(fontSize: 13)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 16, top: 16),
                    child: Column(
                      children: [
                        Container(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 3.5),
                            child: Text(
                              'Out For Delivery',
                              style: AppStyles.boder,
                            ),
                          ),
                          height: 31,
                          width: 126,
                          decoration: BoxDecoration(
                            color: AppColors.backgroundorange,
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(
                              color: AppColors.orange,
                              width: 3,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(
                    color: AppColors.border,
                    width: 1,
                  ),
                ),
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 145, bottom: 2),
                              child: Text(
                                'Estimated For Delivery',
                                style: AppStyles.title_home,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 47),
                              child: Text(
                                'Mon, 10/16/2022 4:30 PM',
                                style: AppStyles.discription_home
                                    .copyWith(fontSize: 18),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  right: 175, bottom: 2, top: 12),
                              child: Text(
                                'Delivery Address',
                                style: AppStyles.title_home,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              child: Text(
                                '777 Brockton Avenue, Abington MA',
                                style: AppStyles.discription_home,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          decoration: BoxDecoration(
            color: AppColors.background,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(
              color: AppColors.frame,
              width: 3,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 12, left: 16, right: 16),
          height: 170,
          width: 382,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16, top: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 100),
                              child: Text('Order Placed',
                                  style: AppStyles.title_home),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 5),
                              child: Text('Mon, 16 Oct 2022 8:00 AM',
                                  style: AppStyles.discription_home
                                      .copyWith(fontSize: 13)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 16, top: 16),
                    child: Column(
                      children: [
                        Container(
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 3.5),
                            child: Text(
                              'Out For Delivery',
                              style: AppStyles.boder,
                            ),
                          ),
                          height: 31,
                          width: 126,
                          decoration: BoxDecoration(
                            color: AppColors.backgroundorange,
                            borderRadius: BorderRadius.circular(30),
                            border: Border.all(
                              color: AppColors.orange,
                              width: 3,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(
                    color: AppColors.border,
                    width: 1,
                  ),
                ),
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 16),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 145, bottom: 2),
                              child: Text(
                                'Estimated For Delivery',
                                style: AppStyles.title_home,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 47),
                              child: Text(
                                'Mon, 10/16/2022 4:30 PM',
                                style: AppStyles.discription_home
                                    .copyWith(fontSize: 18),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  right: 175, bottom: 2, top: 12),
                              child: Text(
                                'Delivery Address',
                                style: AppStyles.title_home,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              child: Text(
                                '777 Brockton Avenue, Abington MA',
                                style: AppStyles.discription_home,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          decoration: BoxDecoration(
            color: AppColors.background,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(
              color: AppColors.frame,
              width: 3,
            ),
          ),
        ),
      ]),
    );
  }
}
