import 'dart:convert';

LoginModel loginModelFromJson(String str) =>
    LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  LoginModel({
    required this.success,
    required this.data,
    required this.metadata,
  });

  bool success;
  Data data;
  Metadata metadata;

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        success: json["success"],
        data: Data.fromJson(json["data"]),
        metadata: Metadata.fromJson(json["metadata"]),
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "data": data.toJson(),
        "metadata": metadata.toJson(),
      };
}

class Data {
  Data({
    required this.id,
    required this.email,
    required this.firstName,
    required this.lastName,
    required this.accessToken,
    required this.refreshToken,
    required this.expiredAt,
    required this.phoneNumber,
    required this.hasDefaultStore,
    required this.defaultStore,
    required this.isEnableNotification,
    required this.createdAt,
    required this.versionApp,
    required this.isDeleted,
  });

  String id;
  dynamic email;
  String firstName;
  String lastName;
  String accessToken;
  String refreshToken;
  DateTime expiredAt;
  String phoneNumber;
  bool hasDefaultStore;
  DefaultStore defaultStore;
  bool isEnableNotification;
  int createdAt;
  String versionApp;
  bool isDeleted;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        email: json["email"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        accessToken: json["access_token"],
        refreshToken: json["refresh_token"],
        expiredAt: DateTime.parse(json["expired_at"]),
        phoneNumber: json["phone_number"],
        hasDefaultStore: json["has_default_store"],
        defaultStore: DefaultStore.fromJson(json["default_store"]),
        isEnableNotification: json["is_enable_notification"],
        createdAt: json["created_at"],
        versionApp: json["version_app"],
        isDeleted: json["is_deleted"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "email": email,
        "first_name": firstName,
        "last_name": lastName,
        "access_token": accessToken,
        "refresh_token": refreshToken,
        "expired_at": expiredAt.toIso8601String(),
        "phone_number": phoneNumber,
        "has_default_store": hasDefaultStore,
        "default_store": defaultStore.toJson(),
        "is_enable_notification": isEnableNotification,
        "created_at": createdAt,
        "version_app": versionApp,
        "is_deleted": isDeleted,
      };
}

class DefaultStore {
  DefaultStore({
    required this.id,
    required this.name,
    required this.npi,
    required this.companyName,
    required this.address,
  });

  String id;
  String name;
  String npi;
  String companyName;
  Address address;

  factory DefaultStore.fromJson(Map<String, dynamic> json) => DefaultStore(
        id: json["id"],
        name: json["name"],
        npi: json["npi"],
        companyName: json["company_name"],
        address: Address.fromJson(json["address"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "npi": npi,
        "company_name": companyName,
        "address": address.toJson(),
      };
}

class Address {
  Address({
    required this.id,
    required this.address1,
    required this.city,
    required this.country,
    required this.fullAddress,
    required this.latitude,
    required this.longitude,
    required this.number,
    required this.postalCode,
    required this.street,
    required this.state,
    required this.isDefault,
    required this.apartment,
  });

  String id;
  String address1;
  String city;
  String country;
  String fullAddress;
  double latitude;
  double longitude;
  String number;
  String postalCode;
  String street;
  String state;
  bool isDefault;
  String apartment;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        id: json["id"],
        address1: json["address1"],
        city: json["city"],
        country: json["country"],
        fullAddress: json["full_address"],
        latitude: json["latitude"].toDouble(),
        longitude: json["longitude"].toDouble(),
        number: json["number"],
        postalCode: json["postal_code"],
        street: json["street"],
        state: json["state"],
        isDefault: json["is_default"],
        apartment: json["apartment"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "address1": address1,
        "city": city,
        "country": country,
        "full_address": fullAddress,
        "latitude": latitude,
        "longitude": longitude,
        "number": number,
        "postal_code": postalCode,
        "street": street,
        "state": state,
        "is_default": isDefault,
        "apartment": apartment,
      };
}

class Metadata {
  Metadata();

  factory Metadata.fromJson(Map<String, dynamic> json) => Metadata();

  Map<String, dynamic> toJson() => {};
}
