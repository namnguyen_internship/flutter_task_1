import 'dart:convert';

List<OrderModel> OrderModelFromJson(String str) =>
    List<OrderModel>.from(json.decode(str).map((x) => OrderModel.fromJson(x)));

String OrderModelToJson(List<OrderModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OrderModel {
  OrderModel({
    required this.id,
    required this.externalId,
    required this.status,
    required this.stage,
    required this.statusName,
    required this.stageName,
    required this.destination,
    required this.recipient,
    required this.taskDetails,
    required this.store,
    required this.isSignatureRequired,
    required this.deliveryDistance,
    required this.distanceUnit,
    required this.pickupStartTime,
    this.pickupEndTime,
    this.pickupInstructions,
    this.dropoffStartTime,
    required this.dropoffEndTime,
    required this.deliveryInstructions,
    required this.scheduledPickupTime,
    required this.orderValue,
    required this.orderDetails,
    required this.isSpirit,
    required this.deliveryContactNotifySms,
    required this.isHazardous,
    required this.isPermissionToLeaveAtDoor,
    required this.isRx,
    required this.hasRefrigeratedItems,
    required this.hasPerishableItems,
    required this.availableToUpdate,
    required this.isOtcOrder,
    required this.stageColor,
    required this.statusColor,
    this.orderBgColor,
    required this.createdAt,
  });

  String id;
  String externalId;
  String status;
  String stage;
  String statusName;
  String stageName;
  Destination destination;
  Recipient recipient;
  TaskDetails taskDetails;
  Store store;
  bool isSignatureRequired;
  double deliveryDistance;
  String distanceUnit;
  int pickupStartTime;
  dynamic pickupEndTime;
  dynamic pickupInstructions;
  dynamic dropoffStartTime;
  int dropoffEndTime;
  String deliveryInstructions;
  int scheduledPickupTime;
  double orderValue;
  List<OrderDetail> orderDetails;
  bool isSpirit;
  bool deliveryContactNotifySms;
  bool isHazardous;
  bool isPermissionToLeaveAtDoor;
  bool isRx;
  bool hasRefrigeratedItems;
  bool hasPerishableItems;
  bool availableToUpdate;
  bool isOtcOrder;
  String stageColor;
  String statusColor;
  dynamic orderBgColor;
  int createdAt;

  factory OrderModel.fromJson(Map<String, dynamic> json) => OrderModel(
        id: json["id"],
        externalId: json["external_id"],
        status: json["status"],
        stage: json["stage"],
        statusName: json["status_name"],
        stageName: json["stage_name"],
        destination: Destination.fromJson(json["destination"]),
        recipient: Recipient.fromJson(json["recipient"]),
        taskDetails: TaskDetails.fromJson(json["task_details"]),
        store: Store.fromJson(json["store"]),
        isSignatureRequired: json["is_signature_required"],
        deliveryDistance: json["delivery_distance"].toDouble(),
        distanceUnit: json["distance_unit"],
        pickupStartTime: json["pickup_start_time"],
        pickupEndTime: json["pickup_end_time"],
        pickupInstructions: json["pickup_instructions"],
        dropoffStartTime: json["dropoff_start_time"],
        dropoffEndTime: json["dropoff_end_time"],
        deliveryInstructions: json["delivery_instructions"],
        scheduledPickupTime: json["scheduled_pickup_time"],
        orderValue: json["order_value"],
        orderDetails: List<OrderDetail>.from(
            json["order_details"].map((x) => OrderDetail.fromJson(x))),
        isSpirit: json["is_spirit"],
        deliveryContactNotifySms: json["delivery_contact_notify_sms"],
        isHazardous: json["is_hazardous"],
        isPermissionToLeaveAtDoor: json["is_permission_to_leave_at_door"],
        isRx: json["is_rx"],
        hasRefrigeratedItems: json["has_refrigerated_items"],
        hasPerishableItems: json["has_perishable_items"],
        availableToUpdate: json["available_to_update"],
        isOtcOrder: json["is_otc_order"],
        stageColor: json["stage_color"],
        statusColor: json["status_color"],
        orderBgColor: json["order_bg_color"],
        createdAt: json["created_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "external_id": externalId,
        "status": status,
        "stage": stage,
        "status_name": statusName,
        "stage_name": stageName,
        "destination": destination.toJson(),
        "recipient": recipient.toJson(),
        "task_details": taskDetails.toJson(),
        "store": store.toJson(),
        "is_signature_required": isSignatureRequired,
        "delivery_distance": deliveryDistance,
        "distance_unit": distanceUnit,
        "pickup_start_time": pickupStartTime,
        "pickup_end_time": pickupEndTime,
        "pickup_instructions": pickupInstructions,
        "dropoff_start_time": dropoffStartTime,
        "dropoff_end_time": dropoffEndTime,
        "delivery_instructions": deliveryInstructions,
        "scheduled_pickup_time": scheduledPickupTime,
        "order_value": orderValue,
        "order_details":
            List<dynamic>.from(orderDetails.map((x) => x.toJson())),
        "is_spirit": isSpirit,
        "delivery_contact_notify_sms": deliveryContactNotifySms,
        "is_hazardous": isHazardous,
        "is_permission_to_leave_at_door": isPermissionToLeaveAtDoor,
        "is_rx": isRx,
        "has_refrigerated_items": hasRefrigeratedItems,
        "has_perishable_items": hasPerishableItems,
        "available_to_update": availableToUpdate,
        "is_otc_order": isOtcOrder,
        "stage_color": stageColor,
        "status_color": statusColor,
        "order_bg_color": orderBgColor,
        "created_at": createdAt,
      };
}

class Destination {
  Destination({
    required this.id,
    required this.address1,
    required this.city,
    required this.country,
    required this.fullAddress,
    required this.latitude,
    required this.longitude,
    required this.number,
    required this.postalCode,
    required this.street,
    required this.state,
    required this.isDefault,
    required this.apartment,
  });

  String id;
  String address1;
  String city;
  String country;
  String fullAddress;
  double latitude;
  double longitude;
  String number;
  String postalCode;
  String street;
  String state;
  bool isDefault;
  String apartment;

  factory Destination.fromJson(Map<String, dynamic> json) => Destination(
        id: json["id"],
        address1: json["address1"],
        city: json["city"],
        country: json["country"],
        fullAddress: json["full_address"],
        latitude: json["latitude"].toDouble(),
        longitude: json["longitude"].toDouble(),
        number: json["number"],
        postalCode: json["postal_code"],
        street: json["street"],
        state: json["state"],
        isDefault: json["is_default"],
        apartment: json["apartment"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "address1": address1,
        "city": city,
        "country": country,
        "full_address": fullAddress,
        "latitude": latitude,
        "longitude": longitude,
        "number": number,
        "postal_code": postalCode,
        "street": street,
        "state": state,
        "is_default": isDefault,
        "apartment": apartment,
      };
}

class OrderDetail {
  OrderDetail({
    required this.id,
    required this.pharmaceuticalName,
  });

  String id;
  String pharmaceuticalName;

  factory OrderDetail.fromJson(Map<String, dynamic> json) => OrderDetail(
        id: json["id"],
        pharmaceuticalName: json["pharmaceutical_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "pharmaceutical_name": pharmaceuticalName,
      };
}

class Recipient {
  Recipient({
    required this.id,
    this.email,
    required this.firstName,
    required this.lastName,
    required this.phoneNumber,
  });

  String id;
  dynamic email;
  String firstName;
  String lastName;
  String phoneNumber;

  factory Recipient.fromJson(Map<String, dynamic> json) => Recipient(
        id: json["id"],
        email: json["email"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        phoneNumber: json["phone_number"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "email": email,
        "first_name": firstName,
        "last_name": lastName,
        "phone_number": phoneNumber,
      };
}

class Store {
  Store({
    required this.id,
    required this.npi,
    required this.name,
    required this.address,
  });

  String id;
  String npi;
  String name;
  Destination address;

  factory Store.fromJson(Map<String, dynamic> json) => Store(
        id: json["id"],
        npi: json["npi"],
        name: json["name"],
        address: Destination.fromJson(json["address"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "npi": npi,
        "name": name,
        "address": address.toJson(),
      };
}

class TaskDetails {
  TaskDetails({
    required this.deliveryProvider,
    this.trackingUrl,
    this.barcodeUrl,
    this.barcodeType,
    this.vehicle,
    this.failureNotes,
    this.failureReason,
    this.notes,
    this.driverId,
    this.driverName,
    this.driverPhoto,
    this.driverPhoneNumber,
    this.signatureImageUrl,
    this.attachments,
    this.actualDeliveryTime,
    this.estimatedPickupTime,
    this.estimatedDeliveryTime,
    this.feedback,
    this.trackingNumber,
    required this.amount,
    required this.currencyCode,
    this.labelLink,
  });

  String deliveryProvider;
  dynamic trackingUrl;
  dynamic barcodeUrl;
  dynamic barcodeType;
  dynamic vehicle;
  dynamic failureNotes;
  dynamic failureReason;
  dynamic notes;
  dynamic driverId;
  dynamic driverName;
  dynamic driverPhoto;
  dynamic driverPhoneNumber;
  dynamic signatureImageUrl;
  dynamic attachments;
  dynamic actualDeliveryTime;
  dynamic estimatedPickupTime;
  dynamic estimatedDeliveryTime;
  dynamic feedback;
  dynamic trackingNumber;
  double amount;
  String currencyCode;
  dynamic labelLink;

  factory TaskDetails.fromJson(Map<String, dynamic> json) => TaskDetails(
        deliveryProvider: json["delivery_provider"],
        trackingUrl: json["tracking_url"],
        barcodeUrl: json["barcode_url"],
        barcodeType: json["barcode_type"],
        vehicle: json["vehicle"],
        failureNotes: json["failure_notes"],
        failureReason: json["failure_reason"],
        notes: json["notes"],
        driverId: json["driver_id"],
        driverName: json["driver_name"],
        driverPhoto: json["driver_photo"],
        driverPhoneNumber: json["driver_phone_number"],
        signatureImageUrl: json["signature_image_url"],
        attachments: json["attachments"],
        actualDeliveryTime: json["actual_delivery_time"],
        estimatedPickupTime: json["estimated_pickup_time"],
        estimatedDeliveryTime: json["estimated_delivery_time"],
        feedback: json["feedback"],
        trackingNumber: json["tracking_number"],
        amount: json["amount"],
        currencyCode: json["currency_code"],
        labelLink: json["label_link"],
      );

  Map<String, dynamic> toJson() => {
        "delivery_provider": deliveryProvider,
        "tracking_url": trackingUrl,
        "barcode_url": barcodeUrl,
        "barcode_type": barcodeType,
        "vehicle": vehicle,
        "failure_notes": failureNotes,
        "failure_reason": failureReason,
        "notes": notes,
        "driver_id": driverId,
        "driver_name": driverName,
        "driver_photo": driverPhoto,
        "driver_phone_number": driverPhoneNumber,
        "signature_image_url": signatureImageUrl,
        "attachments": attachments,
        "actual_delivery_time": actualDeliveryTime,
        "estimated_pickup_time": estimatedPickupTime,
        "estimated_delivery_time": estimatedDeliveryTime,
        "feedback": feedback,
        "tracking_number": trackingNumber,
        "amount": amount,
        "currency_code": currencyCode,
        "label_link": labelLink,
      };
}
